<?php
    require 'database.php';

	$nomError = $precioError = $desError= $deptoError = $catError = $cantidadError = null;

    if( !empty($_POST)){
        $nombre = $_POST['nombre'];
        $precio = $_POST['precio'];
        $descripcion = $_POST['descripcion'];
        $departamento = $_POST['departamento'];
        $categoria = $_POST['categoria'];
		$cantidad = $_POST['cantidad'];
		
        $valid = true;

        if(empty($nombre)){
            $nomError = 'Escriba un nombre';
            $valid = false;
        }

        if(empty($precio)){
            $precioError = 'Escriba un precio';
            $valid = false;
        }

        if(empty($descripcion)){
            $desError = 'Escriba una descripcion';
            $valid = false;
        }

        if(empty($departamento)){
            $deptoError = 'Seleccione un departamento';
            $valid = false;
        }

        if(empty($categoria)){
            $catError = 'Seleccione una categoria';
            $valid = false;
		}
		
		if(empty($cantidad)){
            $cantidadError = 'Seleccione una cantidad';
            $valid = false;
        }

        if ($valid) {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO `productos`(`nombre`, `precio`, `descripcion`, `departamento_id`, `categoria_id`, `cantidad`) VALUES (?, ?, ?, ? , ?, ?)";			
			$q = $pdo->prepare($sql);
			$q->execute(array($nombre,$precio,$descripcion,$departamento,$categoria,$cantidad));			
			Database::disconnect();
			header("Location: inventario.php");
		}
    }
?>
<!DOCTYPE html>
<html lang="en">      
    <head>
    <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
	<title>Añadir producto</title>
    </head>
    <body>
    <div class="container">
        <div>
            <div class="row">
                <h3>Agregar un nuevo producto al inventario</h3>
            </div>
            <form class="form-horizontal" action="create_producto.php" method="post">

                <div class="form-group <?php echo !empty($nomError)?'error':'';?>">
					<label class="control-label">Nombre del producto</label>
	    			<div class="controls">
				      	<input class="form-control" name="nombre" type="text"  placeholder="nombre" value="<?php echo !empty($nombre)?$nombre:'';?>">
				      	<?php if (($nomError != "")) ?>
				      	<span class="help-inline"><?php echo $nomError;?></span>						      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($precioError)?'error':'';?>">
					<label class="control-label">Precio del producto</label>
	    			<div class="controls">
				      	<input class="form-control" name="precio" type="number" step=0.01 min=0  placeholder="precio" value="<?php echo !empty($precio)?$precio:'';?>">
				      	<?php if (($precioError != "")) ?>
				      	<span class="help-inline"><?php echo $precioError;?></span>						      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($desError)?'error':'';?>">
					<label class="control-label">Descripcion del producto</label>
	    			<div class="controls">
				      	<input class="form-control" name="descripcion" type="text"  placeholder="Describa el producto" value="<?php echo !empty($descripcion)?$descripcion:'';?>">
				      	<?php if (($desError != "")) ?>
				      	<span class="help-inline"><?php echo $desError;?></span>				      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($deptoError)?'error':'';?>">
				    <label class="control-label">Departamento</label>
					<div class="controls">
	                   	<select name ="departamento" class="form-control">
		                    <option value="">Selecciona un departamento</option>
	                        <?php
						   		$pdo = Database::connect();
								$query = 'SELECT * FROM departamento';
			 			   		foreach ($pdo->query($query) as $row) {
		                    		if ($row['id']==$departamento)
		                    			echo "<option selected value='" . $row['id'] . "'>" . $row['nombre'] . "</option>";
		                    		else
		                    			echo "<option value='" . $row['id'] . "'>" . $row['nombre'] . "</option>";
		   						}
		   						Database::disconnect();
			  				?>
                        </select>
					  	<?php if (($deptoError) != null) ?>
				      		<span class="help-inline"><?php echo $deptoError;?></span>
					</div>
				</div>

                <div class="form-group <?php echo !empty($catError)?'error':'';?>">
				    <label class="control-label">Categoria</label>
					<div class="controls">
	                   	<select name ="categoria" class="form-control">
		                    <option value="">Selecciona una categoria</option>
	                        <?php
						   		$pdo = Database::connect();
								$query = 'SELECT * FROM categoria';
			 			   		foreach ($pdo->query($query) as $row) {
		                    		if ($row['id']==$categoria)
		                    			echo "<option selected value='" . $row['id'] . "'>" . $row['nombre'] . "</option>";
		                    		else
		                    			echo "<option value='" . $row['id'] . "'>" . $row['nombre'] . "</option>";
		   						}
		   						Database::disconnect();
			  				?>
                        </select>
					  	<?php if (($catError) != null) ?>
				      		<span class="help-inline"><?php echo $catError;?></span>
					</div>
				</div>

				<div class="form-group <?php echo !empty($cantidadError)?'error':'';?>">
					<label class="control-label">Cantidad de unidades</label>
	    			<div class="controls">
				      	<input class="form-control" name="cantidad" type="number" min=1  placeholder="Cantidad" value="<?php echo !empty($cantidad)?$cantidad:'';?>">
				      	<?php if (($cantidadError != "")) ?>
				      	<span class="help-inline"><?php echo $cantidadError;?></span>						      	
				    </div>
				</div>

                <div class="form-actions">
						<button type="submit" class="btn btn-success">Agregar</button>
						<a class="btn" href="inventario.php">Regresar</a>
					</div>
            </form>
        </div>
    </div>
    </body>
</html>