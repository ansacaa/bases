<?php
    require 'database.php';

    if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
    }
    
	if ( null==$id ) {
		header("Location: inventario.php");
    }

    $especieError = $sexoError = $desError= $razaError = $fdnError = $precioError = $altoError = $anchoError = $largoError= $cantidadError =null;

    if( !empty($_POST)){
        $especie = $_POST['especie'];
        $sexo = $_POST['sexo'];
        $descripcion = $_POST['descripcion'];
        $raza = $_POST['raza'];
        $fdn = $_POST['fdn'];
        $precio = $_POST['precio'];
        $alto = $_POST['alto'];
        $ancho = $_POST['ancho'];
        $largo = $_POST['largo'];
        $cantidad = $_POST['cantidad'];

        $valid = true;

        if(empty($especie)){
            $especieError = 'Seleccione una especie';
            $valid = false;
        }

        if(empty($precio)){
            $precioError = 'Escriba un precio';
            $valid = false;
        }

        if(empty($descripcion)){
            $desError = 'Escriba una descripcion';
            $valid = false;
        }

        if(empty($sexo)){
            $sexoError = 'Escriba el sexo';
            $valid = false;
        }

        if(empty($raza)){
            $razaError = 'Escriba la raza';
            $valid = false;
        }

        if(empty($fdn)){
            $fdnError = 'Escriba la fecha de nacimiento';
            $valid = false;
        }

        if(empty($alto)){
            $altoError = 'Especifique la altura que ocupa';
            $valid = false;
        }

        if(empty($ancho)){
            $anchoError = 'Especifique el ancho que ocupa';
            $valid = false;
        }

        if(empty($largo)){
            $largoError = 'Especifique lo largo que ocupa';
            $valid = false;
        }

        if(empty($cantidad)){
            $cantidadError = 'Seleccione una cantidad';
            $valid = false;
        }

        if ($valid) {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE `animales` SET `especie_id`=?, `descripcion`=?, `sexo`=?, `altura`=?, `ancho`=?, `largo`=?, `raza`=?, `fecha_de_nacimiento`=?, `precio`=?, `cantidad`=? WHERE id = ?";			
			$q = $pdo->prepare($sql);
			$q->execute(array($especie,$descripcion,$sexo,$alto,$ancho,$largo,$raza,$fdn,$precio,$cantidad,$id));			
			Database::disconnect();
			header("Location: inventario.php");
		}
    } else {
        $pdo = Database::connect();
	    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM `animales` WHERE id=?";			
        $query = $pdo->prepare($sql);
        $query->execute(array($id));
        $q = $query->fetch(PDO::FETCH_BOTH);
        $especie = $q['especie_id'];
        $sexo = $q['sexo'];
        $descripcion = $q['descripcion'];
        $raza = $q['raza'];
        $fdn = $q['fecha_de_nacimiento'];
        $precio = $q['precio'];
        $alto = $q['altura'];
        $ancho = $q['ancho'];
        $largo = $q['largo'];
        $cantidad = $q['cantidad'];
        Database::disconnect();
    }
?>
<!DOCTYPE html>
<html lang="en">      
    <head>
    <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
	<title>Añadir Animal</title>
    </head>
    <body>
    <div class="container">
        <div>
            <div class="row">
                <h3>Agregar un nuevo animal al refugio</h3>
            </div>
            <form class="form-horizontal" action="update_animal.php?id=<?php echo $id?>" method="post">
                
                <div class="form-group <?php echo !empty($especieError)?'error':'';?>">
		    		<label class="control-label">Especie</label>
			    	<div class="controls">
	                   	<select name ="especie" class="form-control">
	                        <option value="">Selecciona la especie</option>
                            <?php
						   	    $pdo = Database::connect();
    							$query = 'SELECT * FROM especie';
	    		 		   		foreach ($pdo->query($query) as $row) {
		                    		if ($row['id']==$especie)
		                    			echo "<option selected value='" . $row['id'] . "'>" . $row['nombre'] . "</option>";
		                    		else
	                        			echo "<option value='" . $row['id'] . "'>" . $row['nombre'] . "</option>";
	   					    	}
		   					    Database::disconnect();
			  			    ?>
                        </select>
    				  	<?php if (($especieError) != null) ?>
	    		      		<span class="help-inline"><?php echo $especieError;?></span>
		    		</div>
			    </div>

                <div class="form-group <?php echo !empty($sexoError)?'error':'';?>">
					<label class="control-label">Sexo del animal</label>
	    			<div class="controls">
				      	<input class="form-control" name="sexo" type="text"  placeholder="Sexo" value="<?php echo !empty($sexo)?$sexo:'';?>">
				      	<?php if (($sexoError != "")) ?>
                          <span class="help-inline"><?php echo $sexoError;?></span>				      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($razaError)?'error':'';?>">
					<label class="control-label">Raza del animal</label>
	    			<div class="controls">
				      	<input class="form-control" name="raza" type="text"  placeholder="Raza" value="<?php echo !empty($raza)?$raza:'';?>">
				      	<?php if (($razaError != "")) ?>
				      	<span class="help-inline"><?php echo $razaError;?></span>						      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($precioError)?'error':'';?>">
					<label class="control-label">Precio del animal</label>
	    			<div class="controls">
				      	<input class="form-control" name="precio" type="number" step=0.01 min=0  placeholder="Precio" value="<?php echo !empty($precio)?$precio:'';?>">
				      	<?php if (($precioError != "")) ?>
				      	<span class="help-inline"><?php echo $precioError;?></span>						      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($desError)?'error':'';?>">
					<label class="control-label">Descripción del animal</label>
	    			<div class="controls">
				      	<input class="form-control" name="descripcion" type="text"  placeholder="Describa al animal" value="<?php echo !empty($descripcion)?$descripcion:'';?>">
				      	<?php if (($desError != "")) ?>
				      	<span class="help-inline"><?php echo $desError;?></span>				      	
				    </div>
				</div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="control-label">Altura</label>
                        <input type="number" step=0.01 class="form-control" name="alto" placeholder="(En metros)" value="<?php echo !empty($alto)?$alto:'';?>">
                        <?php if(($altoError != "")) ?>
                        <span class="help-inline"><?php echo $altoError;?></span>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label">Ancho</label>
                        <input type="number" step=0.01 min=0 class="form-control" name="ancho" placeholder="(En metros)" value="<?php echo !empty($ancho)?$ancho:'';?>">
                        <?php if(($anchoError != "")) ?>
                        <span class="help-inline"><?php echo $anchoError;?></span>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label">Largo</label>
                        <input type="number" step=0.01 min=0 class="form-control" name="largo" placeholder="(En metros)" value="<?php echo !empty($largo)?$largo:'';?>">
                        <?php if(($largoError != "")) ?>
                        <span class="help-inline"><?php echo $largoError;?></span>
                    </div>
                </div>

                <div class="form-group <?php echo !empty($fdnError)?'error':'';?>">
					<label class="control-label">Fecha de nacimiento</label>
	    			<div class="controls">
				      	<input class="form-control" name="fdn" type="date"  placeholder="YYYY/MM/DD" value="<?php echo !empty($fdn)?$fdn:'';?>">
				      	<?php if (($fdnError != "")) ?>
				      	<span class="help-inline"><?php echo $fdnError;?></span>				      	
				    </div>
				</div>

                <div class="form-group <?php echo !empty($cantidadError)?'error':'';?>">
					<label class="control-label">Cantidad de animales</label>
	    			<div class="controls">
				      	<input class="form-control" name="cantidad" type="number" min=1  placeholder="Cantidad" value="<?php echo !empty($cantidad)?$cantidad:'';?>">
				      	<?php if (($cantidadError != "")) ?>
				      	<span class="help-inline"><?php echo $cantidadError;?></span>						      	
				    </div>
				</div>

                <div class="form-actions">
						<button type="submit" class="btn btn-primary">Actualizar</button>
						<a class="btn" href="inventario.php">Regresar</a>
					</div>
            </form>
        </div>
    </div>
    </body>
</html>