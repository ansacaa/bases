<?php require "database.php";  
    $products = array();
    $animals = array();

    if(array_key_exists('confirm', $_POST)){
        echo("Entra");
        try {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo("CONECTA");
        
            $pdo->beginTransaction();
        
            $sql = "INSERT INTO `ventas`(`personal_id`, `cliente_id`, `total`, `subtotal`, `iva`, `impuesto`) VALUES (?,?,?,?,?,?)";			
            $q = $pdo->prepare($sql);
            $q->execute(array($_POST['personal_id'], 1,0,0,0,0));	
            $lastInsert = $pdo->lastInsertId();
            echo($lastInsert);
            $total = 0;
            
            foreach ($_POST as $key => $value) {
                if($key == 'confirm' || $key == 'personal_id') continue;
                if(substr($key, 0, 7) == 'product'){
                    $producto_id = $value;
                    if(!array_key_exists('quantity_product_' . $producto_id, $_POST) || $_POST['quantity_product_' . $producto_id] == "") continue;

                    $sql = "INSERT INTO `producto_ventas`(`producto_id`, `venta_id`, `cantidad`) VALUES (?,?,?)";			
                    $q = $pdo->prepare($sql);
                    $q->execute(array($producto_id, $lastInsert, $_POST['quantity_product_' . $producto_id]));

                    $sql2 = "SELECT precio, cantidad FROM `productos` WHERE id = ?";
                    $query = $pdo->prepare($sql2);
                    $query->execute(array($producto_id));
                    $esp = $query->fetch(PDO::FETCH_BOTH);
                    $total += $esp['precio']*intval($_POST['quantity_product_' . $producto_id]);

                    $sql2 = "UPDATE `productos` SET cantidad = ? WHERE id = ?";			
                    $query = $pdo->prepare($sql2);
                    $query->execute(array($esp['cantidad'] - intval($_POST['quantity_product_' . $producto_id]), $producto_id));

                }elseif(substr($key, 0, 6) == 'animal'){
                    $animal_id = $value;
                    if(!array_key_exists('quantity_animal_' . $animal_id, $_POST) || $_POST['quantity_animal_' . $animal_id] == ""  || $_POST['quantity_animal_' . $animal_id] == "0") continue;

                    $sql = "INSERT INTO `animales_ventas`(`animal_id`, `venta_id`, `cantidad`) VALUES (?,?,?)";			
                    $q = $pdo->prepare($sql);
                    $q->execute(array($animal_id, $lastInsert, $_POST['quantity_animal_' . $animal_id]));
                    
                    $sql2 = "SELECT precio, cantidad FROM `animales` WHERE id = ?";
                    $query = $pdo->prepare($sql2);
                    $query->execute(array($animal_id));
                    $esp = $query->fetch(PDO::FETCH_BOTH);
                    $total += $esp['precio']*intval($_POST['quantity_animal_' . $animal_id]);

                    $sql2 = "UPDATE `animales` SET cantidad = ? WHERE id = ?";			
                    $query = $pdo->prepare($sql2);
                    $query->execute(array($esp['cantidad'] - intval($_POST['quantity_animal_' . $animal_id]), $animal_id));

                }
            }

            $sql = "UPDATE `ventas`SET total = ?, subtotal = ?, iva = ? WHERE id = ?";			
            $q = $pdo->prepare($sql);
            $q->execute(array($total, $total/1.16, $total*0.16/1.16, $lastInsert));
            $pdo->commit();
        } 
        catch (\Exception $e) {
            if ($pdo->inTransaction()) {
                $pdo->rollback();
                // If we got here our two data updates are not in the database
            }
            echo($e);
        }
        
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head> 
        <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
        <title>Caja</title>
    </head>
    <body>
    <div class="menu-wrapper center-relative">
        <nav id="header-main-menu">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="index.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="caja.php">Caja</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inventario.php">Inventario</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ultimasventas.php">Últimas ventas</a>
                </li>
                </ul>
        </nav>
    </div>
    <div class="container">
        <form action="caja.php" method="post">
        <div class="row justify-content-center">
                <div class="col-10">
                    <div class="form-group">
                        <label for="personal"><h3>¿Quien realiza la venta?</h3></label>
                        <select class="form-control" id="personal" name="personal_id" required>
                            <option>Seleccionar vendedor</option>
                        <?php 
                            $pdo = Database::connect();
                            $sql = "SELECT id, nombre, apellido_paterno, apellido_materno FROM `personal`";
                            $personal = $pdo->query($sql);
                            foreach ($personal as $row) {
                                echo("<option value='" . $row['id'] . "'>" . $row['nombre']. " " . $row['apellido_paterno'] . " " . $row['apellido_materno'] . "</option>");
                            }
                        ?>
                        </select>
                    </div>
                </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-10">
                <div>
                    <h2>Lista de productos y animales en el inventario</h2><br>
                </div>
                <div>
                    <h3>Productos</h3>
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Folio</td>
                            <td>Nombre</td>
                            <td>Precio</td>
                            <td>Cantidad</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $pdo = Database::connect();
                        $sql = "SELECT * FROM `productos`";
                        $prod = $pdo->query($sql);
                        foreach ($prod as $row) {
                            echo "<tr>";
                            echo "<td>". $row["id"] ."</td>";
                            echo "<td>". $row["nombre"] ."</td>";
                            echo "<td>$". $row["precio"] ."</td>";
                            echo "<input type=\"hidden\" name='product_" . $row['id'] ."' value='" . $row['id'] . "'/>";
                            echo "<td><input class='form-control form-control-sm' type='number' min='0' max='" . $row['cantidad'] . "' name='quantity_product_" . $row['id'] . "'/></td>";
                            echo "</tr>";
                        }
                    ?>
                    </tbody>
                    
                    </table>
                </div>
                <div>
                    <h3>Animales</h3>
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Folio</td>
                            <td>Especie</td>
                            <td>Raza</td>
                            <td>Precio</td>
                            <td>Cantidad</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $pdo = Database::connect();
                        $sql = "SELECT * FROM `animales`";
                        $prod = $pdo->query($sql);
                        foreach ($prod as $row) {
                            echo "<tr>";
                            echo "<td>". $row["id"] ."</td>";
                            $sql = "SELECT * FROM `especie` WHERE id = ?";
                            $query = $pdo->prepare($sql);
                            $query->execute(array($row["especie_id"]));
                            $esp = $query->fetch(PDO::FETCH_BOTH);
                            echo "<td>". $esp["nombre"] ."</td>";
                            echo "<td>". $row["raza"] ."</td>";
                            echo "<td>$". $row["precio"] ."</td>";
                            echo "<input type=\"hidden\" name='animal_" . $row['id'] ."' value='" . $row['id'] . "'/>";
                            echo "<td><input class='form-control form-control-sm' type='number' min='0' max='" . $row['cantidad'] . "' name='quantity_animal_" . $row['id'] . "'/></td>";
                            echo "</tr>";
                        }
                    ?>
                    
                    </tbody>
                    </table>
                </div>
                <input type="hidden" name="confirm" value=1/>
                <button class="btn btn-outline-success btn-block mb-5" type="submit">Concretar venta</button>
            </div>
        </div>
        </form>
    </div>
    </body>
</html>