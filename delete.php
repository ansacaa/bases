<?php 
	require 'database.php';
	$id = $type = 0;	
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];	
	}
	if ( !empty($_GET['type'])) {
		$type = $_REQUEST['type'];	
	}


	if ( !empty($_POST)) {
		// keep track post values		
        $id = $_POST['id'];	
		$type = $_POST['type'];
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if($type =="animales"){
			$sql = "DELETE FROM animales WHERE id = ?";
		}else{
			$sql = "DELETE FROM productos WHERE id = ?";
		}
		$q = $pdo->prepare($sql);
		$q->execute(array($id));		
		Database::disconnect();
		header("Location: inventario.php");		
	} 	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
    <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
	</head>

	<body>
	    <div class="container">	    
	    	<div class="span10 offset1">
	    		<div class="row">
			    	<h3>Eliminar producto</h3>
			    </div>
			    
			    <form class="form-horizontal" action="delete.php" method="post">
		    		<input type="hidden" name="id" value="<?php echo $id;?>"/>
					<input type="hidden" name="type" value="<?php echo $type;?>"/>
					<p class="alert alert-error">Estas seguro que quieres eliminar este elemento de los <?php echo $type ?> ?</p>
					<div class="form-actions">
						<button type="submit" class="btn btn-danger">Si</button>
						<a class="btn" href="inventario.php">No</a>
					</div>
				</form>
			</div>					
	    </div> <!-- /container -->
	</body>
</html>