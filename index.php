<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
        <title>Bienvenido a +Kota</title>
    </head>
    <body>
    <div class="menu-wrapper center-relative">
        <nav id="header-main-menu">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="caja.php">Caja</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inventario.php">Inventario</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ultimasventas.php">Últimas ventas</a>
                </li>
                </ul>
        </nav>
    </div>
        <div id="home" class="text-center">
            <div class="block content-1170 center-relative center-text">
                <br/><br/><br/>
                <img class="top-logo" src="images/maskota-logo.jpeg" />
                <br/><br/><br/>
		        <h1 h1 class="big-title">Bienvenido a +Kota</h1>
                <br/><br/>
		        <p class="title-desc">La mejor tienda de mascotas de todo el pais</p>
		    </div>
        </div>
    </body>
</html>