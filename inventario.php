<?php require "database.php";  ?>
<!DOCTYPE html>
<html lang="en">
    <head> 
        <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
        <title>Inventario</title>
    </head>
    <body>
    <div class="menu-wrapper center-relative">
        <nav id="header-main-menu">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="index.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="caja.php">Caja</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inventario.php">Inventario</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ultimasventas.php">Últimas ventas</a>
                </li>
                </ul>
        </nav>
    </div>
    <div class="container">
        <div>
            <h2>Lista de productos y animales en el inventario</h2><br>
        </div>
        <div>
            <h3>Productos</h3>
            <a class="btn btn-success" href="create_producto.php">Añadir Producto</a>
            <br><br>
            <table class="table table-striped">
            <thead>
                <tr>
                    <td>Folio</td>
                    <td>Nombre</td>
                    <td>Precio</td>
                    <td>Descripcion</td>
                    <td>Departamento</td>
                    <td>Categoria</td>
                    <td>Cantidad</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </thead>
            <tbody>
            <?php
                $pdo = Database::connect();
                $sql = "SELECT * FROM `productos`";
                $prod = $pdo->query($sql);
                foreach ($prod as $row) {
                    echo "<tr>";
                    echo "<td>". $row["id"] ."</td>";
                    echo "<td>". $row["nombre"] ."</td>";
                    echo "<td>". $row["precio"] ."</td>";
                    echo "<td>". $row["descripcion"] ."</td>";
                    $sql = "SELECT * FROM `departamento` WHERE id = ?";
                    $query = $pdo->prepare($sql);
					$query->execute(array($row["departamento_id"]));
					$dep = $query->fetch(PDO::FETCH_BOTH);
                    echo "<td>". $dep["nombre"] ."</td>";
                    $sql = "SELECT * FROM `categoria` WHERE id = ?";
                    $query = $pdo->prepare($sql);
					$query->execute(array($row["categoria_id"]));
					$cat = $query->fetch(PDO::FETCH_BOTH);
                    echo "<td>". $cat["nombre"]."</td>";
                    echo "<td>". $row["cantidad"] ."</td>";
                    echo "<td><a class=\"btn btn-primary\" href=\"update_producto.php?id=".$row["id"]."\">Editar</a></td>";
                    echo "<td><a class=\"btn btn-danger\" href=\"delete.php?id=".$row["id"]."&type=productos\">Borrar</a></td>";
                    echo "</tr>";
                }
            ?>
            </tbody>
            
            </table>
        </div>
        <div>
            <h3>Animales</h3>
            <a class="btn btn-success" href="create_animal.php">Añadir Animal</a>
            <br><br>
            <table class="table table-striped">
            <thead>
                <tr>
                    <td>Folio</td>
                    <td>Especie</td>
                    <td>Raza</td>
                    <td>Descripcion</td>
                    <td>Fecha de Nacimiento</td>
                    <td>Precio</td>
                    <td>Cantidad</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </thead>
            <tbody>
            <?php
                $pdo = Database::connect();
                $sql = "SELECT * FROM `animales`";
                $prod = $pdo->query($sql);
                foreach ($prod as $row) {
                    echo "<tr>";
                    echo "<td>". $row["id"] ."</td>";
                    $sql = "SELECT * FROM `especie` WHERE id = ?";
                    $query = $pdo->prepare($sql);
					$query->execute(array($row["especie_id"]));
					$esp = $query->fetch(PDO::FETCH_BOTH);
                    echo "<td>". $esp["nombre"] ."</td>";
                    echo "<td>". $row["raza"] ."</td>";
                    echo "<td>". $row["descripcion"] ."</td>";
                    echo "<td>". $row["fecha_de_nacimiento"] ."</td>";
                    echo "<td>". $row["precio"] ."</td>";
                    echo "<td>". $row["cantidad"] ."</td>";
                    echo "<td><a class=\"btn btn-primary\" href=\"update_animal.php?id=".$row["id"]."\">Editar</a></td>";
                    echo "<td><a class=\"btn btn-danger\" href=\"delete.php?id=".$row["id"]."&type=animales\">Borrar</a></td>";
                    echo "</tr>";
                }
            ?>
            </tbody>
            </table>
        </div>
    </div>
    </body>
</html>