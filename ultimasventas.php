<?php require "database.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="bootstrap-4.2.1-dist/css/bootstrap.min.css">
        <title>Ventas</title>
    </head>
    <body>
    <div class="menu-wrapper center-relative">
        <nav id="header-main-menu">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="index.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="caja.php">Caja</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inventario.php">Inventario</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ultimasventas.php">Últimas ventas</a>
                </li>
                </ul>
        </nav>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <div>
                    <h3>Ultimas Ventas</h3>
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Folio</td>
                            <td>Vendedor</td>
                            <td>Subtotal</td>
                            <td>I.V.A.</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $pdo = Database::connect();
                        $sql = "SELECT * FROM `ventas`";
                        $prod = $pdo->query($sql);
                        $count = 0;
                        foreach ($prod as $row) {
                            echo "<tr>";
                            echo "<td>". $row["id"] ."</td>";
                            $sql = "SELECT * FROM `personal` WHERE id = ?";
                            $query = $pdo->prepare($sql);
                            $query->execute(array($row["personal_id"]));
                            $per = $query->fetch(PDO::FETCH_BOTH);
                            echo "<td>". $per["nombre"] . " " . $per['apellido_paterno'] . " " . $per['apellido_materno'] ."</td>";
                            echo "<td>$". $row["subtotal"] ."</td>";
                            echo "<td>$". $row["iva"] ."</td>";
                            echo "<td>$". $row["total"] ."</td>";
                            echo "</tr>";
                            $count++;
                            if($count == 10) break;
                        }
                    ?>
                    </tbody>
                    
                    </table>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>